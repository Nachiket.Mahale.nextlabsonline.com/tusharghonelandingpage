import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { InstructorComponent } from './instructor/instructor.component';
import { HomeComponent } from './home/home.component';
import { AchievementsComponent } from './achievements/achievements.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';

@NgModule({
  declarations: [
    MainComponent,
    HeaderComponent,
    FooterComponent,
    InstructorComponent,
    HomeComponent,
    AchievementsComponent,
    TestimonialsComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    MainComponent
  ]  
})
export class MainModule { }
